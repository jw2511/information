import functools

from flask import session, g, current_app

from info.models import User


def do_index_class(index):
    """自定义过滤器,过滤点击排序html的class"""
    if index == 1:
        return 'first'
    elif index == 2:
        return 'second'
    elif index == 3:
        return 'third'
    else:
        return ''

# 如果用户登录就获取用户登录数据，没有登录返回None，可以进行代码抽取
# 使用装饰器去加载用户数据并记录到 g 变量
# 在当前请求中可以直接使用 g 变量取到用户数据

# g 作为 flask 程序全局的一个临时变量,充当者中间媒介的作用,
# 我们可以通过它传递一些数据，g 保存的是当前请求的全局变量，
# 不同的请求会有不同的全局变量，通过不同的thread id区别


def user_login_data(f):
    # 装饰内层函数时 保持当前装饰器的__name__不变
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        # 获取到当前登录用户的id
        user_id = session.get('user_id')
        user = None
        # 通过id获取用户信息
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        # 把查询数据给g变量
        g.user = user
        return f(*args, **kwargs)
    return wrapper
