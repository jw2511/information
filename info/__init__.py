from logging.handlers import RotatingFileHandler

from flask import Flask
import logging
# 用来指定Session保存的位置 (大写的)
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy

from redis import StrictRedis
from config import config
from flask_wtf import CSRFProtect
from flask_wtf.csrf import generate_csrf

# 初始化数据库
# 在Flask很多扩展里面都可以先初始化扩展 在去调用 init_app 方法初始化


db = SQLAlchemy()
redis_store = None  # type: StrictRedis


# 变量的注释  作用:方便在用的时候出现提示


def setup_log(config_name):
    # 设置日志的记录等级
    logging.basicConfig(level=config[config_name].LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


def create_app(config_name):
    # 配置日志 传入配置名字 以便能够获取制定配置所对应的日志等级
    setup_log(config_name)
    # 创建Flask对象
    app = Flask(__name__)
    # 加载配置
    app.config.from_object(config[config_name])
    # 通过app初始化
    db.init_app(app)
    # 初始化redis 存储对象
    # 局部变量上升为全局变量
    global redis_store
    redis_store = StrictRedis(host=config[config_name].REDIS_HOST, port=config[config_name].REDIS_PORT,
                              decode_responses=True)
    # 开启当前项目 CSRF 保护 只做服务器验证
    CSRFProtect(app)

    # CSRFProtect 从cookie中取出随机值 从表单取出 对比 响应校验结果
    # 我们要做 1界面加载时 添加csrf_token 2表单中添加隐藏的csrf_token
    # 响应中统一设置cookie 请求钩子after_response
    # 通过ajax请求 不通过表单 通过herders请求头

    @app.after_request
    def after_request(response):
        # 生成一个随机的csrf_token
        csrf_token = generate_csrf()
        # 设置一个cookie
        response.set_cookie('csrf_token', csrf_token)

        return response

    # 设session保存指定位置
    Session(app)
    # 注册蓝图
    # 解决循环导入 在哪使用在哪导入
    from info.modules.index import index_blue
    app.register_blueprint(index_blue)

    from info.modules.passport import passport_blu
    app.register_blueprint(passport_blu)

    from info.modules.news import news_blu
    app.register_blueprint(news_blu)

    from info.modules.profile import profile_blu
    app.register_blueprint(profile_blu)
    # 自定义过滤器
    # 第一个参数是函数名，第二个参数是自定义的过滤器名称
    # 防止循环导入报错 db不能导
    from info.utils.common import do_index_class
    app.add_template_filter(do_index_class, 'index_class')

    from info.modules.admin import admin_blu
    app.register_blueprint(admin_blu)

    from info.utils.common import user_login_data
    from flask import render_template, g

    @app.errorhandler(404)
    @user_login_data
    def page_not_fount(e):
        # 传入data保持页面正常跳转 传入user保证登录功能
        user = g.user
        data = {"user": user.to_dict() if user else None}
        return render_template('news/404.html', data=data)

    return app

    # return app
