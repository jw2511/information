from flask import Blueprint, session, request, url_for, redirect

admin_blu = Blueprint('admin',__name__,url_prefix='/admin')
from . import views


# 只有admin函数 才调用
@admin_blu.before_request
def check_admin():
    is_admin = session.get('is_admin', False)
    # if 不是管理员 and 当前访问的url不是管理登录页:
    # 普通用户不能进入后台管理页面
    if not is_admin and not request.url.endswith(url_for('admin.login')):
        return redirect('/')