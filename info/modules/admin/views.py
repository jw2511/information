import time
from datetime import datetime, timedelta

from flask import render_template, request, jsonify, current_app, session, url_for, redirect, g, abort

from info import constants, db
from info.models import User, News, Category
from info.utils.common import user_login_data
from info.utils.image_storage import storage
from info.utils.response_code import RET
from . import admin_blu


# 用户登录 以及权限控制
@admin_blu.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        # 判断当前是否有登录，如果有登录直接重定向到管理员后台主页
        user_id = session.get('user_id', None)
        is_admin = session.get("is_admin", False)
        if user_id and is_admin:
            return redirect(url_for('admin.index'))

        return render_template('admin/login.html')

    # 取到登录的参数
    username = request.form.get('username')
    password = request.form.get('password')

    # 判断参数
    if not all([username, password]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    try:
        user = User.query.filter(User.mobile == username, User.is_admin == True).first()
    except Exception as e:
        current_app.logger.error(e)
        # 不使用ajax 在当前界面 显示错误信息 不发生跳转
        return render_template('admin/login.html', errmsg='用户信息查询失败')

    if not user:
        return render_template('admin/login.html', errmsg='未查询到用户信息')

    if not user.check_passoword(password):
        return render_template('admin/login.html', errmsg='用户名或者密码错误')

    session['user_id'] = user.id
    session['mobile'] = user.mobile
    session["nick_name"] = user.nick_name
    session["is_admin"] = user.is_admin

    return redirect(url_for('admin.index'))


# 管理员首页
@admin_blu.route('/index')
@user_login_data
def index():
    user = g.user
    return render_template('admin/index.html', user=user.to_dict())


# 用户统计
@admin_blu.route('/user_count')
def user_count():
    # 统计总人数
    total_count = 0
    try:
        total_count = User.query.filter(User.is_admin == False).count()
    except Exception as e:
        current_app.logger.error(e)

    # 获取当前时间
    t = time.localtime()
    # print(t)  time.struct_time(tm_year=2018, tm_mon=12, tm_mday=23,
    # tm_hour=11, tm_min=30, tm_sec=19, tm_wday=6, tm_yday=357,tm_isdst=0)

    # 月新增数
    # create_time > 本月1号
    mon_count = 0
    begin_mon_date_str = '%d-%02d-01' % (t.tm_year, t.tm_mon)
    # 字符串转换为时间对象
    begin_mon_date = datetime.strptime(begin_mon_date_str, "%Y-%m-%d")
    try:
        mon_count = User.query.filter(User.is_admin == False, User.create_time > begin_mon_date).count()
    except Exception as e:
        current_app.logger.error(e)

    # 日新增数
    # create_time > 今天日期
    day_count = 0
    begin_day_date_str = '%d-%02d-%02d' % (t.tm_year, t.tm_mon, t.tm_mday)
    begin_day_data = datetime.strptime(begin_day_date_str, "%Y-%m-%d")
    try:
        day_count = User.query.filter(User.is_admin == False, User.create_time > begin_day_data).count()
    except Exception as e:
        current_app.logger.error(e)

    # 拆线图数据
    active_time = []  # x轴
    active_count = []  # y轴
    # 取到今天的时间字符串
    today_date_str = '%d-%02d-%02d' % (t.tm_year, t.tm_mon, t.tm_mday)
    # 转成时间对象
    today_date = datetime.strptime(today_date_str, '%Y-%m-%d')

    for i in range(0, 31):  # 取不到31
        # 取到某一天的0点0分
        begin_date = today_date - timedelta(days=i)
        # timedelta代表两个datetime之间的时间差
        # 取到下一天的0点0分
        end_date = today_date - timedelta(days=(i - 1))
        count = User.query.filter(User.is_admin == False,
                                  User.last_login >= begin_date, User.last_login < end_date).count()
        # strftime 时间对象转换为字符串
        active_count.append(count)
        active_time.append(begin_date.strftime('%Y-%m-%d'))

    active_count.reverse()
    active_time.reverse()
    # print(active_time)
    # print(active_count)

    data = {
        'total_count': total_count,
        'mon_count': mon_count,
        'day_count': day_count,
        "active_time": active_time,
        "active_count": active_count

    }
    return render_template('admin/user_count.html', data=data)


# 用户列表显示
@admin_blu.route('/user_list')
def user_list():
    page = request.args.get('page', 1)

    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    users = []
    current_page = 1
    total_page = 1

    try:
        paginate = User.query.filter(User.is_admin == False).paginate(page, constants.ADMIN_USER_PAGE_MAX_COUNT, False)
        users = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    user_dict_li = []
    for user in users:
        user_dict_li.append(user.to_admin_dict())

    data = {
        "users": user_dict_li,
        "total_page": total_page,
        "current_page": current_page,
    }

    return render_template('admin/user_list.html', data=data)


# 新闻审核功能  关键字搜索功能
@admin_blu.route('/news_review')
def news_review():
    page = request.args.get('page', 1)
    keywords = request.args.get('keywords', None)

    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    filters = [News.status != 0]
    # 如果关键字存在，那么就添加关键字搜索
    if keywords:
        # 关键字存在 查询标题中是否包含关键字
        filters.append(News.title.contains(keywords))
    news_list = []
    current_page = 1
    total_page = 1

    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()). \
            paginate(page, constants.USER_COLLECTION_MAX_NEWS, False)
        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_review_dict())

    data = {
        'current_page': current_page,
        'total_page': total_page,
        'news_dict_li': news_dict_li
    }

    return render_template('admin/news_review.html', data=data)


# 审核详情
@admin_blu.route('/news_review_detail/<int:news_id>')
def news_review_detail(news_id):
    news = None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)

    # print(news.to_dict())
    if not news:
        return render_template('admin/news_review_detail.html', data={'errmsg': '未查询到此新闻'})

    data = {"news": news.to_dict()}
    return render_template('admin/news_review_detail.html', data=data)


# 新闻内容审核
@admin_blu.route('/news_review_action', methods=['POST'])
def news_review_action():
    # 1. 接受参数
    news_id = request.json.get('news_id')
    action = request.json.get('action')

    # 2. 参数校验
    if not all([news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误1')

    if action not in ('accept', 'reject'):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误2')
    print(news_id)
    print(action)
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询失败')

    print(news)
    if not news:
        return jsonify(errno=RET.NODATA, errmsg='未查询到数据')

    # 代表接受
    if action == 'accept':
        news.status = 0

    # 代表拒绝
    else:
        reason = request.json.get('reason')

        if not reason:
            return jsonify(errno=RET.PARAMERR, errmsg='请输入拒绝原因')
        news.status = -1
        news.reason = reason

    return jsonify(errno=RET.OK, errmsg='OK')


# 新闻版式编辑
@admin_blu.route('/news_edit')
def news_edit():
    page = request.args.get('page', 1)
    keywords = request.args.get('keywords', None)
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    news_list = []
    current_page = 1
    total_page = 1

    filters = [News.status == 0]
    if keywords:
        filters.append(News.title.contains(keywords))
        print(keywords)
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()). \
            paginate(page, constants.USER_COLLECTION_MAX_NEWS, False)

        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    new_dict_li = []
    for news in news_list:
        new_dict_li.append(news.to_basic_dict())

    data = {
        'current_page': current_page,
        'total_page': total_page,
        'new_dict_li': new_dict_li
    }
    return render_template('admin/news_edit.html', data=data)


# 新闻编辑详情页 新闻编辑提交
@admin_blu.route('/news_edit_detail' , methods = ['POST','GET'])
def news_edit_detail():
    if request.method == 'GET':
        news_id = request.args.get('news_id')

        if not news_id:
            abort(404)

        try:
            news_id = int(news_id)
        except Exception as e:
            current_app.logger.error(e)
            return render_template('admin/news_edit_detail.html', errmsg='参数错误')

        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)
            return render_template('admin/news_edit_detail.html', errmsg='查询数据错误')

        if not news:
            return render_template('admin/news_edit_detail.html', errmsg='没有查询到数据1')

        # 查询分类数据
        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
            return render_template('admin/news_edit_detail.html', errmsg='没有查询到数据2')

        category_dict_li = []
        for category in categories:
            # 取到分类的字典
            cate_dict = category.to_dict()
            # 判断当前遍历到的分类是否是当前新闻的分类，如果是，则添加is_selected为true
            if category.id == news.category_id:
                cate_dict["is_selected"] = True
            category_dict_li.append(cate_dict)

        # 移除最新的分类
        category_dict_li.pop(0)

        data = {
            'category_dict_li': category_dict_li,
            'news': news.to_dict()
        }

        return render_template('admin/news_edit_detail.html', data=data)

    # 取到Post进来的数据
    news_id = request.form.get("news_id")
    title = request.form.get("title")
    digest = request.form.get("digest")
    content = request.form.get("content")
    index_image = request.files.get("index_image")
    category_id = request.form.get("category_id")
    print(title)
    print(digest)
    print(content)
    print(category_id)

    # 1.1 判断数据是否有值
    if not all([title, digest, content, category_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数有误")

    # 查询指定id的
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到新闻数据")

    # 1.2 尝试读取图片
    if index_image:
        try:
            index_image = index_image.read()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR, errmsg="参数有误")

        # 2. 将标题图片上传到七牛
        try:
            key = storage(index_image)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.THIRDERR, errmsg="上传图片错误")
        news.index_image_url = constants.QINIU_DOMIN_PREFIX + key

        # 3. 设置相关数据
    news.title = title
    news.digest = digest
    news.content = content
    news.category_id = category_id

    return jsonify(errno=RET.OK, errmsg="OK")


@admin_blu.route('/news_type',methods=['GET','POST'])
def news_type():
    if request.method == 'GET':
        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
            return render_template('admin/news_type.html',errmsg='查询数据错误')

        category_dict_li = []
        for category in categories:
            # 取到分类的字典
            cate_dict = category.to_dict()
            category_dict_li.append(cate_dict)

        # 移除最新的分类
        category_dict_li.pop(0)

        data = {
            'category_dict_li': category_dict_li
        }
        return render_template('admin/news_type.html', data=data)

    # 新增或者添加分类
    # 1. 取参数
    cname = request.json.get('name')
    # 如果传了cid，代表是编辑已存在的分类
    cid = request.json.get('id')

    print(cname)
    print(cid)
    if not cname:
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误1')

    if cid:
        # 有 分类id代表查询相关数据
        try:
            cid = int(cid)
            category = Category.query.get(cid)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR, errmsg='参数错误2')

        if not category:
            return jsonify(errno=RET.NODATA, errmsg='未查询到分类数据')
        category.name = cname

    #   没有cid 表示 直接添加分类
    else:
        category = Category()
        category.name = cname
        db.session.add(category)
    return jsonify(errno=RET.OK, errmsg='OK')

