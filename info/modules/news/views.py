from flask import render_template, g, current_app, abort, jsonify, request

from info import constants, db
from info.models import News, User, Comment, CommentLike
from info.utils.common import user_login_data
from info.utils.response_code import RET

from . import news_blu


@news_blu.route('/<int:news_id>')
@user_login_data
def news_detail(news_id):
    # user_id = session.get('user_id',None)
    # user = None   # type: User
    # if user_id:
    #     try:
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.error(e)
    #     print(user)   # 对象
    #     print(user.to_dict())   # 字典

    # 详情页新闻数据显示

    # 详情页用户数据显示
    # 查询用户登录信息
    user = g.user
    is_collected = False
    is_followed = False

    # 右侧新闻排行逻辑
    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)
    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_basic_dict())

    # 查询新闻数据
    news = None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        abort(404)

    if not news:
        # 返回数据未找到的页面
        # 报404错误，404错误统一显示页面后续再处理
        abort(404)

    # if 用户已经登录 判断是否收藏
    # 如果收藏 is_collected = True
    if g.user:
        if news in g.user.collection_news:
            is_collected = True

    # if 当前新闻有作者，并且 当前登录用户已关注过这个用户
    if news.user and user:
        # if user 是否关注过 news.user
        if news.user in user.followed:
            is_followed = True

    news.clicks += 1

    # 去查询评论数据
    comments = []
    try:
        comments = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)

    comment_like_ids = []
    if user:
        try:
            # 需求：查询当前用户在当前新闻里面都点赞了哪些评论
            # 1. 查询出当前新闻的所有评论 ([COMMENT]) 取到所有的评论id  [1, 2, 3, 4, 5]
            comment_ids = [comment.id for comment in comments]
            # 2. 再查询当前评论中哪些评论被当前用户所点赞
            # （[CommentLike]）查询comment_id 在第1步的评论id列表内的所有数据 & CommentList.user_id = g.user.id
            comment_likes = CommentLike.query.filter(CommentLike.comment_id.in_(comment_ids),
                                                     CommentLike.user_id==user.id).all()
            # 3. 取到所有被点赞的评论id 第2步查询出来是一个 [CommentLike] --> [3, 5]
            comment_like_ids = [comment_like.comment_id for comment_like in comment_likes]
        except Exception as e:
            current_app.logger.error(e)

    comment_dict_li = []

    for comment in comments:
        comment_dict = comment.to_dict()
        # 代表没有点赞
        comment_dict["is_like"] = False
        # 判断当前遍历到的评论是否被当前登录用户所点赞
        if comment.id in comment_like_ids:
            comment_dict["is_like"] = True
        comment_dict_li.append(comment_dict)

        # comment_dict_li.append(comment.to_dict())
    # print(comments)


    data = {
        'user': user.to_dict() if user else None,
        'news': news.to_dict(),
        'news_dict_li': news_dict_li,
        'is_collected': is_collected,
        'comments': comment_dict_li,
        "is_followed": is_followed
    }
    # print(data)

    return render_template('news/detail.html', data=data)


@news_blu.route('/news_collect', methods=['POST'])
@user_login_data
def collect_news():
    """
    1.接受参数
    2.判断参数
    3.查询新闻,判断是否存在
    :return:
    """
    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg='用户未登录')
    # 1.接收参数
    news_id = request.json.get('news_id')
    action = request.json.get('action')
    # print(news_id)
    # print(action)
    # action:string 指定两个值 collect cancel_collect
    # params_dict = json.loads(request.data) = request.json
    # 2.判断参数
    print(action)
    print(news_id)
    if not all([news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    if action not in ["collect", "cancel_collect"]:
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    try:
        news_id = int(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    # 3. 查询新闻，并判断新闻是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询错误')

    if not news:
        return jsonify(errno=RET.NODATA, errmsg='未查询到新闻数据')

    # 4. 收藏以及取消收藏
    # 取消收藏
    if action == 'cancel_collect':
        if news in user.collection_news:
            user.collection_news.remove(news)
    # 收藏
    else:
        print(user.collection_news.all())
        if news not in user.collection_news:
            user.collection_news.append(news)
    # print(user.collection_news)
    return jsonify(errno=RET.OK, errmsg='操作成功')


@news_blu.route('/news_comment', methods=['POST'])
@user_login_data
def comment_news():
    """
    评论新闻 或者新闻下面指定的评论
    :return:
    """
    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg='用户未登录')

    # 1. 取到请求参数
    news_id = request.json.get('news_id')
    comment_content = request.json.get('comment')
    parent_id = request.json.get('parent_id')

    # 2. 判断参数
    print(news_id)
    print(comment_content)
    if not all([news_id,comment_content]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误1')
    try:
        news_id = int(news_id)
        if parent_id:
            parent_id = int(parent_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误2')

    # 查询新闻，并判断新闻是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询错误')

    if not news:
        return jsonify(errno=RET.NODATA, errmsg='未查询到新闻数据')

    # 3. 初始化一个评论模型，并且赋值
    # 添加到数据库 为什么要自己去commit()?，因为在return的时候需要用到 comment 的 id
    comment = Comment()

    comment.user_id = user.id
    comment.news_id = news_id
    comment.content = comment_content
    # print(comment.to_dict())

    if parent_id:
        comment.parent_id = parent_id

    # @app.teardown_appcontext  在每次请求后执行
    # def shutdown_session(response_or_exc):
    #     if app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN']:
    #         if response_or_exc is None:
    #             self.session.commit()
    #  commit 在请求最后自动执行 在这需要手动提交

    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()

    # data = {
    #    "comment": comment.to_dict()
    #
    # }

    return jsonify(errno=RET.OK, errmsg="OK", data=comment.to_dict())


@news_blu.route('/comment_like',methods=['POST'])
@user_login_data
def comment_like():
    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg='用户未登录')

    # 1. 取到请求参数
    comment_id = request.json.get('comment_id')
    action = request.json.get('action')

    # 2. 判断参数
    if not all([comment_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误1')

    if action not in ['add', 'remove']:
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误2')

    try:
        comment_id = int(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误3')

    # 3. 获取到要被点赞的评论模型
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询错误')

    if not comment:
        return jsonify(errno=RET.NODATA, errmsg='评论不存在')

    comment_like_model = CommentLike.query.filter(CommentLike.comment_id == comment_id,
                                                  CommentLike.user_id == user.id).first()

    if action == 'add':

        if not comment_like_model:
            # 点赞评论
            comment_like_model = CommentLike()
            comment_like_model.user_id = user.id
            comment_like_model.comment_id = comment.id
            db.session.add(comment_like_model)
            # db.session.commit()
            comment.like_count += 1

    else:
        # 取消点赞
        if comment_like_model:
            db.session.delete(comment_like_model)
            comment.like_count -= 1

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg='数据库操作失败')

    return jsonify(errno=RET.OK, errmsg='OK')


# 关注与取消关注
@news_blu.route('/followed_user', methods=["POST"])
@user_login_data
def followed_user():

    user = g.user
    print(user)
    if not user:
        print(111)
        return jsonify(errno=RET.SESSIONERR, errmsg="未登录")

    user_id = request.json.get('user_id')
    action = request.json.get('action')
    print(user_id)
    print(action)
    if not all([user_id,action]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误1')

    if action not in("follow", "unfollow"):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误2')

    try:
        other = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询错误')

    if not other:
        return jsonify(errno=RET.NODATA, errmsg='未查询到数据')

    try:
        user_id = int(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误3')

    if action == 'follow':
        # 如果没有关注这个用户
        if user_id == g.user.id:
            return jsonify(errno=RET.PARAMERR, errmsg='自己不需要关注哦')

        if other not in user.followed:
            # 当前用户的关注列表添加一个值
            user.followed.append(other)
        else:
            return jsonify(errno=RET.DATAEXIST, errmsg='当前用户已被关注')

    else:
        # 取消关注
        if other in user.followed:
            user.followed.remove(other)
        else:
            return jsonify(errno=RET.DATAEXIST, errmsg='当前用户未被关注')

    return jsonify(errno=RET.OK, errmsg='操作成功')

