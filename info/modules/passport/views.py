import random
import re
from datetime import datetime

from flask import abort, jsonify, session
from flask import current_app
from flask import make_response
from flask import request


from info import constants, db
from info import redis_store
from info.models import User
from . import passport_blu
from info.utils.captcha.captcha import captcha

from info.lib.yuntongxun.sms import CCP
from info.utils.response_code import RET


@passport_blu.route('/image_code')
def get_image_code():
    """
        生成图片验证码并返回
        1. 取到参数
        2. 判断参数是否有值
        3. 生成图片验证码
        4. 保存图片验证码文字内容到redis
        5. 返回验证码图片
        :return:
    """

    # 1. 取到参数
    # var url = '/passport/image_code?imageCodeId=' + imageCodeId
    # args: 取到url中 ? 后面的参数
    # 'imageCodeId' : imageCodeId  根据键取到值 键名是?imageCodeId=中的
    image_code_id = request.args.get('imageCodeId', None)

    # 2.判断参数是否有值 (异常主动抛出)
    if not image_code_id:
        return abort(403)

    # 3.生成图片验证码
    # 三个返回值 名字 内容 图片
    name, text, image = captcha.generate_captcha()

    # 4.保存图片验证码文字内容到redis (添加操作 set也可以设置过期时间 单位:秒 )
    # 图片验证码Redis有效期， 单位：秒
    # IMAGE_CODE_REDIS_EXPIRES = 300
    try:
        # redis 保存验证码文字内容 图片返回给前端   name名字随意
        redis_store.set('ImageCodeId_' + image_code_id, text, constants.IMAGE_CODE_REDIS_EXPIRES)
        """
         def set(self, name, value, ex=None, px=None, nx=False, xx=False):
            Set the value at key ``name`` to ``value``
            ``ex`` sets an expire flag on key ``name`` for ``ex`` seconds.
        """

    except Exception as e:
        current_app.logger.error(e)
        abort(500)

    # 5.返回验证码图片
    # 不能直接返回image 返回内容和标示内容不一致
    # 设置响应
    response = make_response(image)
    # 设置数据类型 以便浏览器更加只能识别其类型
    response.headers['Content-Type'] = 'image/jpg'
    return response


@passport_blu.route('/sms_code', methods=["POST"])
def send_sms_code():
    """
    发送短信的逻辑
    1. 获取参数：手机号，图片验证码内容，图片验证码的编号 (随机值)
    2. 校验参数(参数是否符合规则，判断是否有值)
    3. 先从redis中取出真实的验证码内容
    4. 与用户的验证码内容进行对比，如果对比不一致，那么返回验证码输入错误
    5. 如果一致，生成验证码的内容(随机数据)
    6. 发送短信验证码
    7. 告知发送结果
    :return:
    """
    '{"mobiel": "18811111111", "image_code": "AAAA", "image_code_id": "u23jksdhjfkjh2jh4jhdsj"}'
    # 1. 获取参数：手机号，图片验证码内容，图片验证码的编号 (随机值)
    # json字典形式字符串不方便取值 转成字典 或者直接取值
    # params_dict = json.loads(request.data)
    # 前端 ajax所返回的数据是json

    params_dict = request.json
    print('.' * 100)
    print(params_dict)
    print('.' * 100)

    mobile = params_dict.get("mobile")
    image_code = params_dict.get("image_code")
    image_code_id = params_dict.get("image_code_id")

    # 2. 校验参数(参数是否符合规则，判断是否有值)
    # 判断参数是否有值
    if not all([mobile, image_code, image_code_id]):
        # {"errno": "4103", "errmsg": "参数有误"}
        return jsonify(errno=RET.PARAMERR, errmsg="参数有误")
    # 校验手机号是否正确
    if not re.match('^1[345678]\\d{9}$', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机号格式不正确")

    # 3. 先从redis中取出真实的验证码内容
    try:
        real_image_code = redis_store.get("ImageCodeId_" + image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")

    if not real_image_code:
        return jsonify(errno=RET.NODATA, errmsg="图片验证码已过期")
    # print(real_image_code.decode(), image_code)
    print(real_image_code, image_code)
    # 在init文件中redis_store中设置decode_responses=True 自动解码
    # 4. 与用户的验证码内容进行对比，如果对比不一致，那么返回验证码输入错误
    # if real_image_code.decode().upper() != image_code.upper():
    if real_image_code.upper() != image_code.upper():
        return jsonify(errno=RET.DATAERR, errmsg="验证码输入错误")

    # 5. 如果一致，生成短信验证码的内容(随机数据)
    # 随机数字 ，保证数字长度为6位，不够在前面补上0
    sms_code_str = "%06d" % random.randint(0, 999999)
    current_app.logger.debug("短信验证码内容是：%s" % sms_code_str)
    # 6. 发送短信验证码  这个验证码单位是分钟 300除以300 有效期一分钟
    # result = CCP().send_template_sms(mobile, [sms_code_str, constants.SMS_CODE_REDIS_EXPIRES / 300], "1")
    # if result != 0:
    #     # 代表发送不成功  第三方返回的状态码 0
    #     return jsonify(errno=RET.THIRDERR, errmsg="发送短信失败")

    # 保存验证码内容到redis
    try:
        redis_store.set("SMS_" + mobile, sms_code_str, constants.SMS_CODE_REDIS_EXPIRES)
        print('数据保存ok^ ^')
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据保存失败")

    # 7. 告知发送结果
    print('send..........100％')
    # 将状态码==0 发送给response 开始倒计时
    return jsonify(errno=RET.OK, errmsg="发送成功")


@passport_blu.route('/register', methods=["POST"])
def register():
    """
    注册的逻辑
    1. 获取参数
    2. 校验参数
    3. 取到服务器保存的真实的短信验证码内容
    4. 校验用户输入的短信验证码内容和真实验证码内容是否一致
    5. 如果一致，初始化 User 模型，并且赋值属性
    6. 将 user 模型添加数据库
    7. 返回响应
    :return:
    """
    # 1.获取参数
    param_dict = request.json
    mobile = param_dict.get('mobile')
    smscode = param_dict.get('smscode')
    password = param_dict.get('password')

    # 2.参数校验
    if not all([mobile, smscode, password]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    if not re.match('^1[345678]\\d{9}$', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg='手机号码输入格式错误')

    # 3.从服务器取到真实的短信内容
    try:
        real_sms_code = redis_store.get('SMS_' + mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询失败')

    if not real_sms_code:
        return jsonify(errno=RET.NODATA, errmsg='验证码过期')

    # 4. 校验用户输入的短信验证码内容和真实验证码内容是否一致
    if real_sms_code != smscode:
        return jsonify(errno=RET.DATAERR, errmsg='验证码输入错误')

    # 5.如果一致,初始化User模型,赋值属性
    user = User()
    user.nick_name = mobile
    user.mobile = mobile
    user.last_login = datetime.now()
    user.password = password     # 用户输入
    # user.password返回models.py的一个对象
    # 对密码做处理
    # 设置password 进行加密处理 并且将加密结果给user.password_hash赋值

    # 6.将 user 模型添加数据库
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DATAERR, errmsg='数据添加失败')

    session['user_id'] = user.id
    session['nick_name'] = user.nick_name
    session['mobile'] = user.mobile

    return jsonify(errno=RET.OK, errmsg='注册成功')


@passport_blu.route('/login', methods=['POST'])
def login():
    """
    1. 获取参数
    2. 校验参数
    3. 校验密码是否正确
    4. 保存用户的登录状态
    5. 响应
    :return:
    """
    # 1. 获取参数
    param_dict = request.json

    mobile = param_dict.get('mobile')
    passport = param_dict.get('password')
    # 2. 校验参数
    if not all([mobile, passport]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')
    # 校验手机号是否正确
    if not re.match('^1[345678]\\d{9}$',mobile):
        return jsonify(errno=RET.PARAMERR, errmsg='手机号码格式错误')

    # 3. 校验密码是否正确
    # 先查询出当前是否有指定手机号的用户
    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询错误')

    # 判断用户是否存在
    if not user:
        return jsonify(errno=RET.NODATA, errmsg='用户不存在')

    # 校验登录的密码和当前用户的密码是否一致
    if not user.check_passoword(passport):
        return jsonify(errno=RET.PWDERR, errmsg='用户名或者密码错误')

    # 4.保存用户信息状态
    session['user_id'] = user.id
    session['mobile'] = user.mobile
    session['nick_name'] = user.nick_name

    #  设置最后一次登陆时间
    user.last_login = datetime.now()

    # 不再主动commit 自动提交 没有rollback()回滚
    # try:
    #     db.session.commit()
    # except Exception as e:
    #     db.session.rollback()
    #     current_app.logger.error(e)
    # 视图函数中 对模型身上的属性有修改 需呀commit到数据库保存
    # 可以不写 需要SQLAlchemy配置

    # 5.响应
    return jsonify(errno=RET.OK, errmsg='登陆成功')


# 退出登录

@passport_blu.route('/logout')
def logout():
    """
    退出登录
    :return:
    """
    # pop是移除session中的数据(dict)
    # pop 会有一个返回值，如果要移除的key不存在，就返回None
    session.pop('user_id', None)
    session.pop('mobile', None)
    session.pop('nick_name', None)
    # 如果不清除 先登录管理员 在登录普通用户 也可以访问管理员页面
    session.pop('is_admin', None)

    return jsonify(errno=RET.OK, errmsg="退出成功")










