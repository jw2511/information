from flask import render_template, request, jsonify, g

from info import constants
from info.models import News, Category
from info.utils.common import user_login_data
from info.utils.response_code import RET
from . import index_blue
from flask import current_app


@index_blue.route('/')
@user_login_data
def index():
    """
     显示首页
    1. 如果用户已经登录，将当前登录用户的数据传到模板中，供模板显示
    :return:
    """
    # 取到用户id
    # user_id = session.get('user_id', None)
    # user = None  # type:User
    # if user_id:
    #     # 查询用户
    #     try:
    #         user = User.query.get(user_id)  # 得到一个对象 不能展示在浏览器
    #
    #     except Exception as e:
    #         current_app.logger.error(e)
    user = g.user
    # g 保存值用于函数的传递
    # 右侧新闻点击排行处理
    # 定义一个空的字典列表，里面装的就是字典
    new_list = []
    try:
        new_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []
    # 遍历对象列表，将对象的字典添加到字典列表中
    # [{new1},{new2},{new3}...]
    for news in new_list:
        news_dict_li.append(news.to_basic_dict())

    # if user:
    # print(user.to_dict())

    # 新闻分类展示
    # 查询分类数据,通过模板形式渲染
    categories = Category.query.all()
    category_li = []
    for category in categories:
        category_li.append(category.to_dict())

    # 三元表达式 有值 dict 没有 none
    # 从对象中获取数据 modules.py中 把字典返回给'user'

    data = {
        "user": user.to_dict() if user else None,
        'news_dict_li': news_dict_li,
        'category_li': category_li
    }

    return render_template('news/index.html', data=data)


@index_blue.route('/news_list')
def news_list():
    """
    获取首页新闻内容
    :return:
    """
    # 1.获取参数
    # 新闻的分类id
    cid = request.args.get('cid', '1')
    page = request.args.get('page', '1')
    per_page = request.args.get('per_page', '10')

    # 2. 验证参数
    try:
        cid = int(cid)
        page = int(page)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    # 普通写法 ==>
    # try:
    #     if cid ==1:
    #         paginate = News.query.[filter()].
    #         order_by(News.create_time.desc()).paginate(page, per_page, False)
    #     else:
    #         paginate = News.query.filter(News.category_id == cid).
    #         order_by(News.create_time.desc()).paginate(page, per_page, False)
    # except Exception as e:
    #     current_app.logger.error(e)
    #     return jsonify(errno=RET.DBERR, errmsg='数据查询错误')

    # 条件拼接 不等于1 时 拼接  提取共同部分
    #  1)定义一个列表存放查询条件
    filters = [News.status == 0]
    #  查询的不是(最新)的数据
    #  2)对参数做判断，符合条件，将查询条件加入空列表
    if cid != 1:
        # 需要添加条件
        # 获得对象列表
        filters.append(News.category_id == cid)   # 查询条件 需要解包传入数据库查询条件

    try:
        #  3)查询时对列表里面内容进行解包
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page, per_page, False)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询错误')

        # 取到当前页的数据
        # paginate返回一个Paginate对象

    news_model_list = paginate.items  # 模型对象列表 当前页数据 列表对象[news1,news2,news3...]
    # print(news_model_list)
    total_page = paginate.pages  # 总页数
    current_page = paginate.page  # 当前页

    news_dict_li = []
    for news in news_model_list:
        news_dict_li.append(news.to_basic_dict())

    data = {
        'total_page': total_page,
        'current_page': current_page,
        'news_dict_li': news_dict_li
    }
    return jsonify(errno=RET.OK, errmsg='OK', data=data)


@index_blue.route('/favicon.ico')
def favicon():
    return current_app.send_static_file('news/favicon.ico')
