import logging

from redis import StrictRedis


class Config(object):
    """项目的配置"""
    DEBUG = True
    # 为mysql添加配置

    SECRET_KEY = 'FhlzDkWAAsvCWwAl8erfWV03jctpp+5sFmskQCytZ2uzUYfn7dkBs/CsD8p9kNYm'
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/information'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # 会自动更新数据库内容 自动commit
    # @app.teardown_appcontext
    # 在每次请求后执行
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    # redis的配置
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379

    # Session保存配置
    SESSION_TYPE = 'redis'
    # 开启session签名  (加密保护)
    SESSION_USE_SIGNER = True
    # 指定Session保存的redis
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    # 设置需要过期  (permanent 永久的)
    SESSION_PERMANENT = False
    # 设置过期时间  (两天)
    PERMANENT_SESSION_LIFETIME = 86400 * 2
    # 设置日志等级
    LOG_LEVEL = logging.DEBUG


class DevelopmentConfig(Config):
    """开发环境下的配置"""
    DEBUG = True


class ProductionConfig(Config):
    """生产环境下的配置"""
    DEBUG = False
    LOG_LEVEL = logging.WARNING


class TestingConfig(Config):
    """单元测试环境下的配置"""
    DEBUG = True
    TESTING = True


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "testing": TestingConfig
}