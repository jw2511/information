import datetime
import random

from flask import current_app
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from info import create_app, db
# 在manage中导入数据模块 否则 数据库迁移没办法完成
from info import models

# 通过指定配置名字创建对应配置app
# create_app 就相当于工厂方法
from info.models import User

app = create_app('development')

manager = Manager(app)
# 将app与db关联
Migrate(app, db)
# 将迁移命令添加到manager中
manager.add_command('db', MigrateCommand)


# 通过命令行执行 传参数
# manager:在终端中可以使用指令来操作程序
# option:可以传递参数
# dest:函数中的参数
# $ python manage.py createsuperuser -n admin -p 112233
@manager.option('-n', '-name', dest='name')
@manager.option('-p', '-password', dest='password')
def createsuperuser(name, password):
    if not all([name, password]):
        print('参数不足')
    user = User()
    user.nick_name = name
    user.mobile = name
    user.password = password
    user.is_admin = True
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)


if __name__ == '__main__':
    # print(app.url_map)
    manager.run()
